/* code8x30x.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegenerator Signetics 8X30x                                             */
/*                                                                           */
/* Historie: 25.6.1997 Grundsteinlegung                                      */
/*                                                                           */
/*****************************************************************************/

extern void code8x30x_init(void);
