/* code4500.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegenerator MELPS-4500                                                  */
/*                                                                           */
/* Historie: 31.12.1996 (23.44!!) Grundsteinlegung                           */
/*                                                                           */
/*****************************************************************************/

extern void code4500_init(void);
