/* codeavr.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegenerator Atmel AVR                                                   */
/*                                                                           */
/* Historie: 26.12.1996 Grundsteinlegung                                     */
/*                                                                           */
/*****************************************************************************/

extern void codeavr_init(void);
