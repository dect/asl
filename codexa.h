/* codexa.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* AS-Codegenerator Philips XA                                               */                    
/*                                                                           */
/* Historie: 25.10.1996 Grundsteinlegung                                     */                           
/*                                                                           */
/*****************************************************************************/

extern void codexa_init(void);
