extern char mytolower(char ch);

extern void BuildTree(char **Patterns);

extern void AddException(char *Name);

extern void DoHyphens(char *word, int **posis, int *posicnt);

extern void DestroyTree(void);
