/* grhyph.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Trennungsmuster deutsch                                                   */
/* abgeleitet von 'ghyph31.tex' aus TeX                                      */
/*                                                                           */
/* Historie: 16.2.1998 Konvertierung                                         */
/*                                                                           */
/*****************************************************************************/

extern char *GRHyphens[];
