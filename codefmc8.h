/* codefmc8.h */
/*****************************************************************************/
/* AS, C-Version                                                             */
/*                                                                           */
/* Codegenerator fuer Fujitsu-F2MC8-Prozessoren                              */
/*                                                                           */
/* Historie:  4.7.1996 Grundsteinlegung                                      */
/*                                                                           */
/*****************************************************************************/

extern void codef2mc8_init(void);
