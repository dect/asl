/* code47c00.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegenerator Toshiba TLCS-47(0(A))                                       */
/*                                                                           */
/* Historie: 30.12.1996 Grundsteinlegung                                     */
/*                                                                           */
/*****************************************************************************/

extern void code47c00_init(void);
