/* code68.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegenerator fuer 68xx-Prozessoren                                       */
/*                                                                           */
/* Historie:  13.8.1996 Grundsteinlegung                                     */
/*                                                                           */
/*****************************************************************************/

extern void code68_init(void);
