/* codemcore.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegenerator MCORE-Familie                                               */
/*                                                                           */
/* Historie:  31.1.1998 Grundsteinlegung                                     */
/*                                                                           */
/*****************************************************************************/

extern void codemcore_init(void);
