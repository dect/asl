/* codecop8.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegeneratormodul COP8-Familie                                           */
/*                                                                           */
/* Historie: 7.10.1996 Grundsteinlegung                                      */
/*                                                                           */
/*****************************************************************************/

extern void codecop8_init(void);
