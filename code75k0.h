/* code75k0.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegenerator NEC 75K0                                                    */
/*                                                                           */
/* Historie: 31.12.1996 Grundsteinlegung                                     */
/*                                                                           */
/*****************************************************************************/

extern void code75k0_init(void);
