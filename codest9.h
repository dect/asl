/* codest9.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegenerator SGS-Thomson ST9                                             */
/*                                                                           */
/* Historie: 10.2.1997 Grundsteinlegung                                      */
/*                                                                           */
/*****************************************************************************/

extern void codest9_init(void);
