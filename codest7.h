/* codest7.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegenerator SGS-Thomson ST7                                             */
/*                                                                           */
/* Historie: 21.5.1997 Grundsteinlegung                                      */
/*                                                                           */
/*****************************************************************************/

extern void codest7_init(void);
