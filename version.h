/* version.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Lagert die Versionsnummer                                                 */
/*                                                                           */
/* Historie: 14.10.1997 Grundsteinlegung                                     */
/*                                                                           */
/*****************************************************************************/

extern char *Version;
extern LongInt VerNo;
