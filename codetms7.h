/* codetms7.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegenerator TMS7000-Familie                                             */
/*                                                                           */
/* Historie: 26.2.1997 Grundsteinlegung                                      */
/*                                                                           */
/*****************************************************************************/

extern void codetms7_init(void);
