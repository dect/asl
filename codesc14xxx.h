/* codesc14xxx.h */
#ifndef _CODESC14XXX_H
#define _CODESC14XXX_H

/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegenerator SC14xxx                                                     */
/*                                                                           */
/* Historie: 25. 3.1999 Grundsteinlegung                                     */
/*                                                                           */
/*****************************************************************************/

extern void codesc14xxx_init(void);

#endif /* _CODESC14XXX_H */
