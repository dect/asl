/* codescmp.h */
/*****************************************************************************/
/* AS-Portierung                                                             */
/*                                                                           */
/* Codegenerator National SC/MP                                              */
/*                                                                           */
/* Historie: 17.2.1996 Grundsteinlegung                                      */
/*                                                                           */
/*****************************************************************************/

extern void codescmp_init(void);
